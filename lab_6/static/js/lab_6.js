//----------------------------------------------------CALCULATOR----------------------------------------------------//
var print = document.getElementById('print');
var erase = false;
var counter = 0;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } else if (x === 'log') {
    print.value = Math.log(print.value);
    erase = true;
  } else if (x === 'sin') {
    print.value = Math.sin(print.value);
    erase = true;
  } else if (x === 'tan') {
    print.value = Math.tan(print.value);
    erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


//chatbox
$(document).ready(function() {
function insertChat(chat) {
  var addedChat;

  
  if (counter % 2 == 0) {	// to determine green or blue
    addedChat = '<div class="msg-send">' + chat + '</div>';	//give css setting to the chat
  } else {
    addedChat = '<div class="msg-receive">' + chat + '</div>';
  }
  counter++;
  $('.msg-insert').append(addedChat);	
}
$(function () {


  $(".chat-text textarea").keypress(function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {		// code value for enter is 13
      e.preventDefault();	//prevent new line
      insertChat(e.target.value);	//call method
      $('textarea').val('');	// to make the typing area blank
      
    }

  });
});


//
// function searchresult() {
//   console.log('searching');
//   //find the result
// }
//
// $("#search").keyup(function(event) {
//   if (event.keyCode == 13) {
//     $("#searchButton").click();
//   }
// });

//theme

  var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
  ]
  var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}


  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  localStorage.setItem("themes",  JSON.stringify(themes));
  alert(localStorage.getItem("selectedTheme"));

  $("body").css("background-color", JSON.parse(localStorage.getItem("selectedTheme")).bcgColor);

  $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem("themes"))
  })

  $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] get value from select .my-select element
    var get = $(".my-select").val();
    var newColor;
    // [TODO] fit the ID theme selected with the theme list
    // var get = document.getElementsByClassName("my-select").id;
    // [TODO] take the selected object theme
    for(i=0; i<themes.length; i++){
      if(themes[i].id == get){
        newColor = themes[i];
      }
    }
    // [TODO] apply modification to entire HTML element which the color need to be changed
    
    // [TODO] save the object theme into local storage selectedTheme
    console.log(newColor);
    localStorage.setItem("selectedTheme", JSON.stringify(newColor));
    console.log(localStorage.getItem("selectedTheme"));


    $("body").css("background-color", newColor.bcgColor);


  })
});
