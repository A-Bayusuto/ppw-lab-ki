# What should you learn from codes & documentation in this sub-directory?

* The implementation of View: the presentation layer to the user, what you see on the browser
* The implementation of Controller: intermediary tier between model and view
* How Django Webserver Works ![How Django Webserver Works](docs/how-django-webserver-works.png)
* How Django Framework Works with Templating ![How Django Framework Works with Templating](docs/how-django-framework-works.png)
* How Django Framework Works with Templating & Models ![How Django Framework Works with Templating & Models](docs/how-django-framework-with-models-works.png)

## Checklist Lab 2

1. Create your _Landing Page_
    1. [ ] Change the content of variabel `landing_page_content` to be your desired _Landing Page_ (Min 30 characters)
    2. [ ] Configure your urls.py file `lab_2/urls.py` so that the _Landing Page_ can be accessed on URL `<HEROKU_APP_HOST>/lab-2/`
        > example : djangoppw.herokuapp.com/lab-2/
2. Create new Django Apps named *lab_2_addon* and configuure your `views.py` in that new Django apps
    1. [ ] Create new `app`  (Hint: Execute  `python manage.py help` to know manage.py available parameters)
    2. [ ] Create/modify your `views.py` on your new `app` using the code below, and make some necessary modifications
    
```python
    from django.shortcuts import render
    from lab_1.views import mhs_name, birth_date
    #Create a list of biodata that you wanna show on webpage:
    #[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
    #TODO Implement
    bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
    {'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
    {'subject' : 'Sex', 'value' : ''}]
    
    def index(request):
        response = {}
        return render(request, 'description_lab2addon.html', response)
```
4. Create a _folder templates_ in `apps lab_2_addon` to keep your HTML/template _file_ HTML for `apps lab_2_addon`:
    1. [ ] Create _folder templates_ in `apps lab_2_addon`
    2. [ ] Copy _file_ `lab_2/templates/description_lab2addon.html` into _folder templates_ in `apps lab_2_addon`
5. Configure your routing so that `description_lab2addon.html` can be accessed properly
    1. [ ] Modify your `urls.py` in _folder_ `lab_2_addon` and `praktikum` for accessing your web application on URL `<HEROKU_APP_HOST>/lab-2-addon/`
    2. [ ] Modify your _section_ INSTALLED_APPS so that `apps lab_2_addon` will be registered as an active _Django Apps_ (if you didn't do this, you may not access `description_lab2addon.html` page that you've already set in `urls.py`)
    3. [ ] Show _Landing Page_ when there is a _request_ to your root _website_ (`<HEROKU_APP_HOST>/`). The _Landing Page_ should appear (Hint: use RedirectView)
    4. [ ] Create a TDD test _file_ in `lab_2_addon/tests.py` by copying from `lab_2/tests.py`. You just need to copy `class Lab2AddonUnitTest` _Test Case_ and activate it by removing `@skip` from every _Test Case_ available.
    Import all `library`, `function` or `variabel` needed in order to run the _Test Case_
        
6. Make sure that you have good _Code Coverage_ 
    1. [ ] If you haven't configure Gitlab to show _Code Coverage_, please read the WIKI about `Show Code Coverage in Gitlab`
    2. [ ] Make sure that you have 100% _Code Coverage_


# References

* Class Material "02. Intro to Framework, Server Side Programming with Django", Slide 1-31
* https://www.djangoproject.com/
* https://docs.djangoproject.com/en/1.11/
* https://docs.djangoproject.com/en/1.11/topics/http/urls/
* https://docs.djangoproject.com/en/1.11/ref/class-based-views/base/#redirectview
